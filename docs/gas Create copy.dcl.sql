/**********************************
 * 								  *
 * 				DCL				  *
 * 								  *
 **********************************/

--CRIAÇÃO DE GRUPOS.
create group grupo_admin;
create group grupo_usuario;

--ADICIONADO PERMISSÕES PARA GRUPO DE ADMINISTRADORES
GRANT all privileges ON ALL TABLES IN SCHEMA public TO grupo_admin;

--ADICIONADO PERMISSÕES PARA GRUPO DE USUARIOS
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO grupo_usuario;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO grupo_usuario;
GRANT all privileges ON relatorio_1 TO grupo_usuario;
GRANT all privileges ON relatorio_2 TO grupo_usuario;
GRANT all privileges ON relatorio_3 TO grupo_usuario;
GRANT all privileges ON relatorio_4 TO grupo_usuario;

--CRIAÇÃO DO USUARIO ADMINISTRADOR E DONO DA BANCO DE DADOS.
create user ohgas_admin with password 'ohgas_admin';
grant grupo_admin to ohgas_admin;
alter database gas owner to ohgas_admin;

--CRIAÇÃO DO USUARIO DO SISTEMA DO BANCO DE DADOS.
create user ohgas_usuario with password 'ohgas_usuario';
grant grupo_usuario to ohgas_usuario;