<?php
namespace api\dao\db;

require_once 'BaseDAO.class.php';
use PDO;
use PDOException;

class RelatorioDAO extends BaseDAO
{

    public function __construct()
    {
        parent::__construct('tb_produto');
    }

    public function r1()
    {
        try {
            $sql = $this->conexao->query("select * from relatorio_1;");
            return $sql->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo "===> Erro relatorio 1: " . $e->getMessage();
        }
    }

    public function r2()
    {
        try {
            $sql = $this->conexao->query("select * from relatorio_2;");
            return $sql->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo "===> Erro ao fazer busca: " . $e->getMessage();
        }
    }

    public function r3()
    {
        try {
            $sql = $this->conexao->query("select * from relatorio_3;");
            return $sql->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo "===> Erro ao executar relatorio 3: " . $e->getMessage();
        }
    }

    public function r4()
    {
        try {
            $sql = $this->conexao->query("select * from relatorio_4;");
            return $sql->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo "===> Erro ao executar relatorio 4: " . $e->getMessage();
        }
    }
}
